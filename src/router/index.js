import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/libros',
    name: 'libros',
    component: () => import('../views/Libros.vue')
  },
  
]

const router = new VueRouter({
  routes
})

export default router
