import axios from 'axios';

const API_URL = 'http://localhost:8080/api/libros/';

class LibroService{
    getListarLibros() {
        return axios.get(API_URL + 'listarLibros');
      }
}

export default new LibroService();